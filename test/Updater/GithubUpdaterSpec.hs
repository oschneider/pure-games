module Updater.GithubUpdaterSpec (spec) where  

import Test.Hspec

import Updater.GithubUpdater

spec :: Spec
spec = do
    describe "GithubUpdater.listReleases" $ do
        it "returns empty if no url given" $ do
            listReleases "" `shouldBe` []
